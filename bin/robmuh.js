#!/usr/bin/env node

let c = {}
 
c.base03 = c.b03 = c.Base03 = c.B03 = '\x1b[1;30m'
c.base02 = c.b02 = c.Base02 = c.B02 = '\x1b[0;30m'
c.base01 = c.b01 = c.Base01 = c.B01 = '\x1b[1;32m'
c.base00 = c.b00 = c.Base00 = c.B00 = '\x1b[1;33m'
c.base0 = c.b0 = c.Base0 = c.B0 = '\x1b[1;34m'
c.base1 = c.b1 = c.Base1 = c.B1 = '\x1b[1;36m'
c.base2 = c.b2 = c.Base2 = c.B2 = '\x1b[0;37m'
c.base3 = c.b3 = c.Base3 = c.B3 = '\x1b[1;37m'
c.yellow = c.y = c.Yellow = c.Y = '\x1b[0;33m'
c.orange = c.o = c.Orange = c.O = '\x1b[1;31m'
c.red = c.r = c.Red = c.R = '\x1b[0;31m'
c.magenta = c.m = c.Magenta = c.M = '\x1b[0;35m'
c.violet = c.v = c.Violet = c.V = '\x1b[1;35m'
c.blue = c.b = c.Blue = c.B = '\x1b[0;34m'
c.cyan = c.c = c.Cyan = c.C = '\x1b[0;36m'
c.green = c.g = c.Green = c.G = '\x1b[0;32m'
c.reset = c.x = c.Reset = c.X = '\x1b[0m'
c.screen = c.clear = c.Screen = c.Clear = c.s = c.S  = '\x1b[2J\x1b[H'
c.line = c.l = c.Line = c.L = '\x1b[2K\x1b[G'

console.log(`
      ${c.r}  __   .__.__            __          __    
  _____|  | _|__|  |   _______/  |______  |  | __
 /  ___/  |/ /  |  |  /  ___/\\   __\\__  \\ |  |/ /
 \\___ \\|    <|  |  |__\\___ \\  |  |  / __ \\|    < 
/____  >__|_ \\__|____/____  > |__| (____  /__|_ \\
     \\/     \\/            \\/            \\/     \\/

  ${c.b1}Rob Muhlestein ${c.b3}/${c.b1} @robmuh
  ${c.g}/^((Found|Teach|Hack)er|(Men|Jani)tor|C\w+O)$/
  ${c.b2}rob@skilstak.com
  ${c.b2}https://skilstak.io
  ${c.b2}704-310-6778
`)